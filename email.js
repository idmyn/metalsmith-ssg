const email = require('emailjs')
const toText = require('html2plaintext')

async function send(html) {
    html = String(html)
    const server = email.server.connect(require('./emailconfig.js'));
    const message = {
	text: toText(html),
	from: "Registrations <registrations@social.coop>",
	to: "Registrations <registrations@social.coop>",
	subject: "Someone has registered! (In the prototype form)",
	attachment:
	[
	    {data: html, alternative: true},
	]
    };
    
    // send the message and get a callback with an error or details of the message that was sent
    server.send(message, function(err, message) { console.log(err || message); });
}


module.exports = send;
