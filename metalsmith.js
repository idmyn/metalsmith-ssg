const Metalsmith  = require('metalsmith')
const assets      = require('metalsmith-assets-improved')
const collections = require('metalsmith-collections')
const layouts     = require('metalsmith-layouts')
const markdown    = require('metalsmith-markdown')
const permalinks  = require('metalsmith-permalinks')
const debug       = require('metalsmith-debug')


module.exports = Metalsmith(__dirname)
      .metadata({
          // use them in layout-files
          sitename: "My Static Site & Blog",
          siteurl: "http://example.com/",
          description: "It's about saying »Hello« to the world.",
          generatorname: "Metalsmith",
          generatorurl: "http://metalsmith.io/"
      })
      .ignore([
          '**/*~',  // Emacs backup files
          '.*',     // dotfiles
      ])
      .clean(true)                // clean destination before
      .use(debug())
      .use(collections({          // group all blog posts by internally
          posts: 'posts/*.md'     // adding key 'collections':'posts'
      }))                         // use `collections.posts` in layouts
      .use(markdown())
      .use(layouts({
          default: 'default.hbs',
      }))
      .use(permalinks({           // change URLs to permalink URLs
          relative: false         // put css only in /css
      }))
      .use(assets())

if (require.main === module) {
    // Called directly, build
    module.exports.build(function(err) {
        if (!err) return
        console.log('Build failed: '+err)
	process.exit(1)
    })
}

