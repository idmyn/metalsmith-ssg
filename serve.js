
const express = require('express')
const metalsmith = require('./metalsmith.js')
const email = require('./email.js')
const shell = require('shelljs')
const webhookOptions = require('./webhook.js')

const app = express()
const port = 9999


var expressOptions = {
  dotfiles: 'ignore',
  etag: false,
//  extensions: ['htm', 'html'],
  index: false,
  maxAge: '1d',
  redirect: false,
  setHeaders: function (res, path, stat) {
    res.set('x-timestamp', Date.now())
  }
}

var fileOptions = {
    root: __dirname + '/build/',
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
    }
}

async function pull() {
    var result = shell.exec('git pull', {cwd: './src'})
    if (result.code !== 0) {
        throw new Error('Git pull failed:'+ result.stderr)
    }
}

async function build() {
    metalsmith.build(function(err) {
        if (err) throw err
    })
}

app.use(express.static('/', expressOptions))
app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.all('*', (req, res, next) => {
    console.log(`${req.method} ${req.url} ${req.ip}`)
    next()    
})
app.get('/', (req, res) => {
    res.redirect('/home')
})
app.get('/style.css', (req, res) => {
    res.sendFile(req.path, fileOptions)
})
app.get('*', (req, res) => {
    res.sendFile(req.path+'/index.html', fileOptions)
})

app.post(webhookOptions.path, (req, res) => {
    var secret = req.header('X-Gitlab-Token')
    if (secret !== webhookOptions.secret) {
	res.sendStatus(403) // Unauthorised
	return
    }

    try {
	build().then(pull) // Async
    }
    catch(e) {
	console.log(e.message)
    }
    res.sendStatus(200)
});

app.post('/registration', (req, res) => {
    console.log('emailing registration: ', req.body);
    try {
	email(`<pre>${JSON.stringify(req.body, null, 2)}</pre>`)
    }
    catch(e) {
	console.log("error sending: ", e);
    }
    
    res.redirect('/home');
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))


